import copy
import os
import pathlib
import unittest
import unittest.mock as mock

import pytest

from pop_config.config.contracts.order import _DefaultOption
from pop_config.config.contracts.order import _get_root
from pop_config.config.contracts.order import _insert_default_placeholders
from pop_config.config.contracts.order import _reroot_path
from pop_config.config.contracts.order import _reroot_paths
from pop_config.config.contracts.order import _restore_raw_defaults


class TestRootsExpansion(unittest.TestCase):
    raw = {
        "idem": {
            "CONFIG": {
                "cache_dir": {"default": "/var/cache/idem"},
                "root_dir": {"default": "/"},
            }
        }
    }

    def test_stash_defaults(self):
        expected_defaults = {"idem": {"cache_dir": "/var/cache/idem", "root_dir": "/"}}
        expected_raw = {
            "idem": {
                "CONFIG": {
                    "cache_dir": {"default": _DefaultOption},
                    "root_dir": {"default": _DefaultOption},
                }
            }
        }

        # assert raw == ret_to_raw(ret)
        # assert raw_default == ret_to_raw(ret, "DEFAULT")
        raw = copy.deepcopy(self.raw)
        defaults = _insert_default_placeholders(raw)
        self.assertEqual(defaults, expected_defaults)
        self.assertEqual(raw, expected_raw)

        _restore_raw_defaults(raw, defaults)
        self.assertEqual(raw, self.raw)

    @pytest.mark.skipif(os.name == "nt", reason="This test only runs on posix systems")
    def test_get_root_posix(self):
        # make sure we get None if root isn't an option:
        self.assertIsNone(_get_root({}, "cli"))

        root = _get_root({"cli": {"no_root"}}, "cli")
        self.assertIsNone(root, None)

        # make sure specified root is retained:
        ret = {"cli": {"root_dir": "myroot"}}
        root = _get_root(ret, "cli")
        self.assertEqual(root, "myroot")

        # make sure we don't create a user-root by default if running as root:
        ret = {"cli": {"root_dir": _DefaultOption}}
        with mock.patch(
            "pop_config.config.contracts.order.os.geteuid",
            autospec=True,
            return_value=0,
        ):
            root = _get_root(ret, "cli")
        self.assertEqual(root, None)

        with mock.patch(
            "pop_config.config.contracts.order.os.geteuid",
            autospec=True,
            return_value=12345,
        ):
            # make sure we don't create a user-root by default if running as root:
            ret = {"cli": {"root_dir": _DefaultOption}}
            root = _get_root(ret, "cli")
        self.assertEqual(root, os.path.expanduser(f"~/.cli"))

    @pytest.mark.skipif(
        os.name != "nt", reason="This test only runs on Windows systems"
    )
    def test_get_root_win(self):
        # make sure we get None if root isn't an option:
        self.assertIsNone(_get_root({}, "cli"))

        root = _get_root({"cli": {"no_root"}}, "cli")
        self.assertIsNone(root, None)

        # make sure specified root is retained:
        ret = {"cli": {"root_dir": "myroot"}}
        root = _get_root(ret, "cli")
        self.assertEqual(root, "myroot")

        # make sure we don't create a user-root by default if running as root:
        ret = {"cli": {"root_dir": _DefaultOption}}
        root = _get_root(ret, "cli")
        assert root is None

    def test_reroot_path_posix(self):
        data = [
            ("/ROOT", "/usr/var/log/MATCH", "/ROOT/usr/var/log/MATCH"),
            ("/ROOT", "/usr/var/log/MATCH/", "/ROOT/usr/var/log/MATCH"),
            ("/ROOT", "/usr/var/log/MATCH/post", "/ROOT/usr/var/log/MATCH/post"),
            ("/ROOT", "/usr/var/log/MATCH/post/", "/ROOT/usr/var/log/MATCH/post"),
            ("/ROOT", "/usr/var/log/MATCH", "/ROOT/usr/var/log/MATCH"),
            ("/ROOT", "relative/path", "relative/path"),
            ("/ROOT/", "/usr/var/log/MATCH", "/ROOT/usr/var/log/MATCH"),
            ("/MULTI/ROOT/", "/MATCH", "/MULTI/ROOT/MATCH"),
        ]
        for root, val, expected in data:
            with mock.patch("pathlib.Path", pathlib.PurePosixPath):
                ret = _reroot_path(val, imp="MATCH", new_root=root)
            self.assertEqual(expected, ret, msg=f"val='{val}', new_root='{root}'")

    def test_reroot_path_win(self):
        data = [
            ("C:\\ROOT", "C:\\usr\\var\\log\\MATCH", "C:\\ROOT\\usr\\var\\log\\MATCH"),
            (
                "C:\\ROOT",
                "C:\\usr\\var\\log\\MATCH\\",
                "C:\\ROOT\\usr\\var\\log\\MATCH",
            ),
            (
                "C:\\ROOT",
                "C:\\usr\\var\\log\\MATCH\\post",
                "C:\\ROOT\\usr\\var\\log\\MATCH\\post",
            ),
            (
                "C:\\ROOT",
                "C:\\usr\\var\\log\\MATCH\\post\\",
                "C:\\ROOT\\usr\\var\\log\\MATCH\\post",
            ),
            ("C:\\ROOT", "C:\\usr\\var\\log\\MATCH", "C:\\ROOT\\usr\\var\\log\\MATCH"),
            ("C:\\ROOT", "relative\\path", "relative\\path"),
            (
                "C:\\ROOT\\",
                "C:\\usr\\var\\log\\MATCH",
                "C:\\ROOT\\usr\\var\\log\\MATCH",
            ),
            ("C:\\MULTI\\ROOT\\", "C:\\MATCH", "C:\\MULTI\\ROOT\\MATCH"),
        ]
        for root, val, expected in data:
            with mock.patch("pathlib.Path", pathlib.PureWindowsPath):
                ret = _reroot_path(val, imp="MATCH", new_root=root)
            self.assertEqual(expected, ret, msg=f"val='{val}', new_root='{root}'")

    def test_reroot_paths_posix(self):
        ret = {
            "replace": {
                "match_abs_dir": "/var/log/replace",
                "match_abs_path": "/var/log/replace/",
                "match_abs_file": "/var/log/replace",
                "nomatch_abs_dir": "/path/to/not_replace",
                "nomatch_absloc_dir": "/path/to/replace_not/loc",
                "nomatch_relative_dir": "relative/replace/dir",
            }
        }
        with mock.patch("pathlib.Path", pathlib.PurePosixPath):
            _reroot_paths(ret, "new_root")
        for k, v in ret["replace"].items():
            if k.startswith("match_"):
                assert v.startswith("new_root"), k
            else:
                assert "new_root" not in v, k

    def test_reroot_paths_win(self):
        ret = {
            "replace": {
                "match_abs_dir": "C:\\var\\log\\replace",
                "match_abs_path": "C:\\var\\log\\replace\\",
                "match_abs_file": "C:\\var\\log\\replace",
                "nomatch_abs_dir": "C:\\path\\to\\not_replace",
                "nomatch_absloc_dir": "C:\\path\\to\\replace_not\\loc",
                "nomatch_relative_dir": "relative\\replace\\dir",
            }
        }
        with mock.patch("pathlib.Path", pathlib.PureWindowsPath):
            _reroot_paths(ret, "new_root")
        for k, v in ret["replace"].items():
            if k.startswith("match_"):
                assert v.startswith("new_root"), k
            else:
                assert "new_root" not in v, k

    def test_reroot_paths_multiple_roots_posix(self):
        defaults = {
            "main": {"root_dir": "/", "other_dir": "/a/main/dir"},
            "other": {"root_dir": "/", "other_dir": "/a/other/dir"},
        }
        expected = {
            "main": {"root_dir": "new_root", "other_dir": "new_root/a/main/dir"},
            "other": {"root_dir": "new_root", "other_dir": "new_root/a/other/dir"},
        }
        with mock.patch("pathlib.Path", pathlib.PurePosixPath):
            _reroot_paths(defaults, "new_root")
        self.assertEqual(expected, defaults)

    def test_reroot_paths_multiple_roots_win(self):
        defaults = {
            "main": {"root_dir": "C:\\", "other_dir": "C:\\a\\main\\dir"},
            "other": {"root_dir": "C:\\", "other_dir": "C:\\a\\other\\dir"},
        }
        expected = {
            "main": {"root_dir": "new_root", "other_dir": "new_root\\a\\main\\dir"},
            "other": {"root_dir": "new_root", "other_dir": "new_root\\a\\other\\dir"},
        }
        with mock.patch("pathlib.Path", pathlib.PureWindowsPath):
            _reroot_paths(defaults, "new_root")
        self.assertEqual(expected, defaults)

    def test_reroot_paths_nochange(self):
        # re-rooting paths the default value ("/") should result in identical values.
        defaults = {"cli": {"root_dir": "/", "other_dir": "/an/other/dir"}}
        expected = copy.deepcopy(defaults)
        with mock.patch("pathlib.Path", pathlib.PurePosixPath):
            _reroot_paths(defaults, "/")
        self.assertEqual(expected, defaults)

    def test_no_default_is_error(self):
        raw = copy.deepcopy(self.raw)
        del raw["idem"]["CONFIG"]["cache_dir"]["default"]
        with pytest.raises(
            KeyError, match="No default value for 'cache_dir' in 'idem's conf.py"
        ):
            _insert_default_placeholders(raw)
