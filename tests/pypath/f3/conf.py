import os

CPATH_DIR = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "config"
)


CLI_CONFIG = {
    "config": {},
}
CONFIG = {
    "config": {
        "default": os.path.join(CPATH_DIR, "default.conf"),
        "help": "The config file path",
    },
}
