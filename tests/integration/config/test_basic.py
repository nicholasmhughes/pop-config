import yaml

# Things left to test:
# Full ordered tests, with all attributes on multiple layers
# CLI arg Groups


def test_source(cli):
    OPT = cli(load=(["m1", "m2"], "m1"))
    assert OPT.m2.base == "single"


def test_load(cli):
    OPT = cli(load=("c1", "c1"))
    assert OPT.c1.int == 2016
    assert OPT.c1.string == "cheese"
    assert OPT.c1.test == 47


def test_render_noargs(cli):
    OPT = cli(load=("rend1", "rend1"))
    assert OPT.rend1.yaml == {"rubber": "Svien"}
    assert OPT.rend1.json == {"rubber": "Svien"}


def test_render_args(cli):
    OPT = cli(
        "--yaml",
        "test: True",
        "--json",
        '{"test": true}',
        "--cli",
        "test=True",
        load=("rend1", "rend1"),
    )
    assert OPT.rend1.yaml == {"test": True}
    assert OPT.rend1.json == {"test": True}
    assert OPT.rend1.cli == [{"test": True}]


def test_os(cli):
    OPT = cli(
        load=("c1", "c1"),
        env={"FOOTEST": "osvar", "POPTESTOSVAR": "good"},
    )
    # Lowercase works
    assert OPT.c1.foo == "osvar"
    # Standard works with cli enabled but not called
    assert OPT.c1.osvar == "good"
    # No OSVAR passed
    assert OPT.c1.lastos == "no os"


def test_os_config(cli, tmp_path):
    """
    test config as an os environment variable
    """
    conf_file = tmp_path / "test"

    with open(conf_file, "w") as fp:
        yaml.safe_dump({"c1": {"config_test": "yes"}}, fp)

    OPT = cli(
        env={"C1_CONFIG": str(conf_file)},
        load=("c1", "c1"),
    )

    assert OPT.c1.config_test == "yes"


def test_log(cli):
    OPT = cli(load=("c1", "c1"))
    assert OPT.c1.log_file


def test_args(cli):
    OPT = cli(
        "--test",
        "new",
        load=("c1", "c1"),
    )
    assert OPT.c1.test == "new"


def test_no_args(cli):
    ret = cli(
        "--foo",
        "new",
        load=("c1", "c1"),
        check=False,
    )

    assert "usage:" in ret
    assert "error: unrecognized arguments: --foo new" in ret


def test_actions(cli):
    OPT = cli(
        "-A",
        load=("c1", "c1"),
    )
    assert OPT.c1.act is True


def test_cli_over_os(cli):
    OPT = cli(
        "--osvar",
        "new",
        env={"FOOTEST": "osvar", "POPTESTOSVAR": "good"},
        load=("c1", "c1"),
    )

    # Lowercase works
    assert OPT.c1.foo == "osvar"
    # Standard works
    assert OPT.c1.osvar == "new"
    # No OSVAR passed
    assert OPT.c1.lastos == "no os"


def test_dyne_cli(cli):
    OPT = cli(
        "--dyned1",
        "DYNED1",
        "--exdyned1",
        "EXDYNED1",
        "--exdyned2",
        "EXDYNED2",
        "--exdyned3",
        "EXDYNED3",
        "--exdyned4",
        "EXDYNED4",
        "ext_test",
        load=("d1", "d1"),
    )
    assert OPT.d1.dyned1 == "DYNED1"
    assert OPT.d1.exdyned1 == "EXDYNED1"
    assert OPT.d1.exdyned2 == "EXDYNED2"
    assert OPT.d1.exdyned3 == "EXDYNED3"
    assert OPT.d1.exdyned4 == "EXDYNED4"


def test_dyne_defaults(cli):
    OPT = cli(load=("d1", "d1"))
    assert OPT.d1.dyned1 == "default"


def test_subcommands_single(cli):
    OPT = cli(
        "test",
        "--single",
        "another!",
        load=("s1", "s1"),
    )
    # assert hub.SUBPARSER == "test"
    assert OPT.s1.single == "another!"


def test_subcommands_double_1(cli):
    OPT = cli("test", "--double", "another!", load=("s1", "s1"))
    assert OPT.s1.double == "another!"


def test_subcommands_double_2(cli):
    ret = cli("valid", "--single", "another!", load=("s1", "s1"), check=False)
    assert "error: unrecognized arguments: --single another!" in ret


def test_subcommands_invalid(cli):
    OPT = cli("test", "--double", "another!", load=("s1", "s1"))
    assert OPT.s1.double == "another!"


def test_positional(cli):
    OPT = cli(
        "foo",
        "bar",
        load=("p1", "p1"),
    )
    assert OPT.p1.first == "foo"
    assert OPT.p1.second == "bar"


def test_version(cli):
    ret = cli(
        "--version",
        load=("c1", "c1"),
        check=False,
    )
    assert ret.strip() == "c1 1"
