=================
CONFIG Dictionary
=================

The ``CONFIG`` dictionary is used to define settings in your application
that may or may not also be settable on the command-line. This means that
the bulk of your application's settings will be defined in the ``CONFIG``
dictionary, even if only to serve as a place to define their ``default``
setting and ``help`` string.

.. code-block:: python

    CONFIG = {
        "name": {
            "default": "frank",
            "help": "Enter the name to use",
        },
    }


This simple example creates a config setting called ``name`` and sets
sets the documentation for the configuration value
and what the default value should be.

If we then wanted to allow ``name`` to be set from the command-line, we
would add a companion entry to ``CLI_CONFIG``, which might look like this:

.. code-block:: python

    CLI_CONFIG = {"name": {}}

We could now set the value of ``name`` via the ``--name foo`` or
``--name=foo`` option.

It is also possible that we may have things in ``CONFIG`` that simply
aren't settable via the command-line and are never intended to be.
These options can be overridden using ``pop-config`` configuration
files (see :ref:`Using Configuration Files`).

Basic Settings
==============

Nearly every config setting needs to have 2 basic options, `default` and
`help`. These are very self explanatory, `default` sets the default value
of the option if no option is passed and `help` presents, not only the
command line help, but is also the single source of documentation for the
option.

Here is a simple example:

.. code-block:: python

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

This establishes the basic data for the setting and is all that is needed for
settings in the *CONFIG* dictionary.

Destination
===========

When the argument is named "test" it will appear on the option namespace as
"test". This may not always be desirable. If the name of the option and where
it needs to be stored differs, then use the `dest` option:

.. code-block:: python

    CONFIG = {
        "test": {
            "default": "Red",
            "dest": "cheese",
            "help": "What color to test",
        },
    }

In this example the option will be stored under the name "cheese", accessible
at ``hub.OPT["my_project"].cheese``.

Location
========

Once the config system has been run, all configuration data will appear in the
`hub.OPT` namespace. This means that in our above example, if the system in
question is part of an app named `myapp`, then the option data will be present
at `hub.OPT["myapp"]["test"]`. Because we use special dictionaries, it is also
possible to access this value as ``hub.OPT.myapp.test``. This works fine as long
as there are no hyphens in your project name or option, in which case the index-based
access method can be used.
