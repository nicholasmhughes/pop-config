.. pop-config documentation master file, created by
   sphinx-quickstart on Wed Mar 11 13:52:53 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

======================
Welcome to pop-config
======================

``pop-config`` is a core component of POP and a key part of POP's ability to "app merge" applications.

The word "config" has a lot of different meanings depending on context, so it's important that we
clarify the capabilities of ``pop-config``. In the world of POP, think of
"config" as referring to *command-line arguments for a program*, *plus a bunch of other cool
functionality related to command-line arguments*. It might be best to pretend ``pop-config`` is
actually named ``pop-args``. This would be a more accurate name and better convey its functionality.

The reason why the name ``pop-config`` is used may be that ``pop-config`` has a lot of very
useful functionality, and taken as a whole, can often be used to manage the entire configuration
of your application. For example, ``pop-config`` allows you to:

* Define command-line arguments for your application.
* Optionally set these command-line arguments via environment variables.
* Optionally source these command-line arguments from a file.
* Merge in plugins which augment the command-line arguments of your application.

The important thing to keep in mind is that all this functionality is *argument-centric*, and
yes, many applications will find their configuration needs fully satisfied by ``pop-config``.

But it's really ``pop-args`` :) Just keep that in the back of your head. It will help.




.. toctree::
   :maxdepth: 2
   :glob:

   topics/pop-config
   tutorial/index
   releases/index

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :caption: Contents:

   topics/introduction
   topics/config
   topics/cli_config
   topics/subcommands
   topics/advanced
   topics/merging
   topics/contributing
   topics/license
   Project Repository <https://gitlab.com/saltstack/pop/pop-create/>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
